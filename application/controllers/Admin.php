<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
        parent::__construct();

        $this->load->database();
    }


	public function index()
	{
		$this->load->view('admin/dashboard');
	}

	public function establishments() {
		$this->load->model('admin_model');

		$data['title'] = "Establishments";
		$data['small_title'] = "Manage list";
		$data['establishments'] = $this->admin_model->get_establishments();

		$this->load->view('admin/establishments', $data);
	}

	public function users() {
		$this->load->model('users_model');
		$data['title'] = "Users";
		$data['small_title'] = "Manage list";
		$data['users'] = $this->users_model->get_users();
		$this->load->view('admin/users', $data);
	}

	public function insert_user(){
		$this->load->model('users_model');
		$this->load->helper(array('form', 'url', 'security'));
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('lname', 'Last Name', 'required|xss_clean');
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]|xss_clean');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|is_unique[users.email]|xss_clean');
		$this->form_validation->set_rules('cel-num', 'Cellphone Number', 'required|is_unique[users.cel-num]|xss_clean');
		$this->form_validation->set_rules('birthdate', 'Birthdate', 'required|xss_clean');
		$this->form_validation->set_rules('status', 'Status', 'required|xss_clean');
		if ($this->form_validation->run() == FALSE) {
			echo validation_errors();
		} else {
			$data = $this->input->post();
			$this->users_model->add_user($data);
			echo 'Data Inserted Successfully';
		}
	} 
}
