<?php $this->load->view('admin/header'); ?>

<style type="text/css">
    td {
        font-size: 12px;
    }
</style>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo ucfirst($title); ?> <small><?php echo ucfirst($small_title); ?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                Home / Dashboard
                            </li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <table id="esttable" class="table table-condensed table-bordered table-striped">
                            <thead>
                                <td><strong>Name</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Option</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Service Type</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Owner</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Phone Number </strong>  <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                                <td><strong>Address</strong> <img class="pull-right" src="<?php echo base_url()?>assets/images/sort_both.png"></td>
                            </thead>
                            <tbody>
                                <?php foreach($establishments as $row): ?>
                                <tr>
                                    <td><?php echo $row['name'] ?></td>
                                    <td><?php echo $row['option'] ?></td>
                                    <td><?php echo $row['service_type'] ?></td>
                                    <td><?php echo $row['owner'] ?></td>
                                    <td><?php echo (isset($row['telephone_number'])?$row['telephone_number']:"").(isset($row['cellphone_number'])?" / ".$row['cellphone_number']:""); ?> </td>
                                    <td><?php ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>

<?php $this->load->view('admin/footer'); ?>

<!-- INSERT JQUERY HERE -->
<script type="text/javascript">
    $(document).ready( function () {
        $('#esttable').DataTable({
            "bJQueryUI": true
        });
    } );
</script>